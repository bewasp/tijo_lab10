package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {
    @Override
    public boolean isRightMovement(FigureMoveDto figureMoveDto) {
        if(FigureType.BISHOP == figureMoveDto.getType()) {
            return checkMovement(figureMoveDto);
        } else {
          return false;
        }
    }

    private boolean checkMovement(FigureMoveDto figureMoveDto) {
        String[] startField = figureMoveDto.getStart().split("_");
        String[] destinationField = figureMoveDto.getDestination().split("_");

        int startRow = startField[0].charAt(0);
        int destinationRow = destinationField[0].charAt(0);
        int startColumn = Integer.parseInt(startField[1]);
        int destinationColumn = Integer.parseInt(destinationField[1]);

        return Math.abs(startRow - destinationRow) == Math.abs(startColumn - destinationColumn);
    }
}
